import '../css/main.scss';
import '@babel/polyfill';
import * as d3 from 'd3';

let partiesButtons = document.getElementsByClassName('btn__party');
let btnReinit = document.getElementsByClassName('btn__reinit')[0];
let barraSumatorio = document.getElementsByClassName('barra--empty')[0];
let currentSum = 0;

let currentArray = [{name: 'empty', color: '#c6c6c6', data: 135}];

let partyDiccionary = [
    {name: 'psc', color: '#f60b01', data: 33},
    {name: 'erc', color: '#f9b33c', data: 33},
    {name: 'jxc', color: '#20c0b2', data: 32},
    {name: 'cs', color: '#f45a09', data: 6},
    {name: 'cup', color: '#fff200', data: 9},
    {name: 'pp', color: '#48aafd', data: 3},
    {name: 'podem', color: '#692772', data: 8},
    {name: 'vox', color: '#77be2d', data: 11}
]

initHemicycle();

function initHemicycle() {
    let width = document.getElementById('hemiciclo').clientWidth,
        height = document.getElementById('hemiciclo').clientHeight;

    let svg = d3.select('#hemiciclo')
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr('class', 'probando')
        .attr("transform", "translate(" + width / 2 + "," + height + ")");

    let radius = height;

    let arc = d3.arc()
        .outerRadius(radius)
        .innerRadius(radius - 80);

    let pie = d3.pie()
        .value(function(d){
            return d.data;
        })
        .sort(null)
        .startAngle(-1 * Math.PI / 2)
        .endAngle(Math.PI / 2);

    let piedData = pie(currentArray);

    let slices = svg.selectAll(`.prueba`)
        .data(piedData)
        .enter()
        .append("path")   
        .style("fill", function(d){ return d.data.color; })
        .style("opacity", .8)        
        //.attr("d", arc)
        .style('stroke', ' #fff')
        .style('stroke-width', '.75px')
        .each(function(d) { this._current = Object.assign({}, d, { endAngle: d.startAngle })});

    slices
        .transition()
        .duration(500)
        .attrTween('d', updateTween);

    //Lógica para eventos de botones
    function addParty(partido) {
        let filterParty = partyDiccionary.filter((item) => item.name == partido)[0];
        currentSum += filterParty.data;

        if(currentArray.length == 1){
            currentArray.splice(0,0, filterParty);
        } else {
            currentArray.splice(currentArray.length - 1,0, filterParty);
        }        

        //Quitamos la suma al empty
        currentArray[currentArray.length - 1].data = currentArray[currentArray.length - 1].data - filterParty.data;        
        
        let piedData = pie(currentArray);

        d3.selectAll('.probando path').remove();

        let slices = svg.selectAll('.prueba')
            .data(piedData)
            .enter()
            .append("path")         
            .style("fill", function(d){ return d.data.color; })
            .style("opacity", .8)        
            .attr("d", arc)
            .style('stroke', ' #fff')
            .style('stroke-width', '.75px')
            //.each(function(d,i) {console.log(d,i); this._current = Object.assign({}, d, { endAngle: d.startAngle })});

        // slices
        //     .transition()
        //     .delay(function(d,i){return i * 150;})
        //     .attrTween('d', updateTween);
    }

    function removeParty(partido) {
        let deletedParty = currentArray.filter((item) => {return item.name == partido})[0];
        currentSum -= deletedParty.data;
        let restArray = currentArray.filter((item) => {return item.name != partido});
        

        //Añadimos la suma al empty
        restArray[restArray.length - 1].data += deletedParty.data;

        currentArray = restArray.slice();
        
        let piedData = pie(currentArray);

        d3.selectAll('.probando path').remove();

        let slices = svg.selectAll('.prueba')
            .data(piedData)
            .enter()
            .append("path")            
            .style("fill", function(d){ return d.data.color; })
            .style("opacity", .8)        
            .attr("d", arc)
            .style('stroke', ' #fff')
            .style('stroke-width', '.75px')
            //.each(function(d) { this._current = Object.assign({}, d, { endAngle: d.startAngle })});

        // slices
        //     .transition()
        //     .delay(function(d,i){return i * 150;})
        //     .attrTween('d', updateTween);
    }

    function reinitHemicycle() {
        currentArray = [{name: 'empty', color: '#c6c6c6', data: 135}];
        currentSum = 0;

        let piedData = pie(currentArray);

        d3.selectAll('.probando path').remove();

        let slices = svg.selectAll(`.prueba`)
            .data(piedData)
            .enter()
            .append("path")   
            .style("fill", function(d){ return d.data.color; })
            .style("opacity", .8)        
            //.attr("d", arc)
            .style('stroke', ' #fff')
            .style('stroke-width', '.75px')
            .each(function(d) { this._current = Object.assign({}, d, { endAngle: d.startAngle })});

        slices
            .transition()
            .delay(function(d,i){return i * 350;})
            .attrTween('d', updateTween);
    }

    function handleParty(party) {
        let nameParty = party.getAttribute('data-name');
        if(party.classList.contains('selected')){            
            removeParty(nameParty);
            party.classList.remove('selected');
        } else {
            addParty(nameParty);
            party.classList.add('selected');
        }
    
        //updateText();
    }

    function updateTween (d) {
        var i = d3.interpolate(this._current, d);
        this._current = i(0);
        return function(t) {
          return arc(i(t));
        };
    }

    //Eventos botones
    for(let i = 0; i < partiesButtons.length; i++){
        let btn = partiesButtons[i];
        btn.addEventListener('click', function(e){            
            handleParty(e.target);
        });
    }

    btnReinit.addEventListener('click', function() {
        reinitHemicycle();
        //updateText();
        //Clases CSS para resto de botones
        for(let i = 0; i < partiesButtons.length; i++) {
            let item = partiesButtons[i];
            if(item.classList.contains('selected')){
                item.classList.remove('selected');
            }
        }
    });    
}

//Lógica específica para botón de Podemos
if ( window.innerWidth <= 420 ) {
    let btnPodem = document.getElementsByClassName("btn__party--podem")[0];
    btnPodem.children[0].innerHTML = "ECP";
}

// function updateText() {
//     let updatedSpan = document.getElementsByClassName('is-updated')[0];
//     if(currentSum > 67){
//         //Barras
//         let partyBars = barraSumatorio.getElementsByTagName('div');
//         for(let i = 0; i < partyBars.length; i++){
//             partyBars[i].style.opacity = 1;
//         }
//         //Texto
//         updatedSpan.textContent = 'sí';
//         if(!updatedSpan.classList.contains('positive')){
//             updatedSpan.classList.toggle('positive')
//         }
//     } else {
//         //Barras
//         let partyBars = barraSumatorio.getElementsByTagName('div');
//         for(let i = 0; i < partyBars.length; i++){
//             partyBars[i].style.opacity = .5;
//         }
//         //Texto
//         updatedSpan.textContent = 'no';
//         if(updatedSpan.classList.contains('positive')){
//             updatedSpan.classList.toggle('positive')
//         }
//     }
// }